#ifdef _OPENMP
#include <omp.h>
#endif

#include <stdio.h>

int main(void) {
    #ifdef _OPENMP
        printf("Version de OpenMP = %d\n", _OPENMP);
        printf("# de procesadores: %d\n", omp_get_num_procs());
        printf("# de threads: %d\n", omp_get_num_threads());
    #else
        printf("No hay soporte de OpenMP :-(\n");
    #endif
    return 0;
}
