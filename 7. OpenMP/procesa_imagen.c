// Convierte a tonos de gris una imagen PNG.

#include <stdio.h>
#include <stdint.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

struct rgb {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

int main(void) {
    int ancho, altura, canales;
    struct rgb *data = (struct rgb *) stbi_load(
        "natalie.png", &ancho, &altura, &canales, 0);
    if (data != NULL) {
        printf("Ancho = %d\n", ancho);
        printf("Alto = %d\n", altura);
        printf("Canales = %d\n", canales);
        int num_pixels = ancho * altura;
        
        #ifdef _OPENMP
        printf("Usando OpenMP\n");
        #endif
        
        #pragma omp parallel for num_threads(1)
        for (int j = 0; j < 1000; j++)
            for (int i = 0; i < num_pixels; i++) {
                int gray = (data[i].red
                            + data[i].green
                            + data[i].blue) / 3;
                data[i].red = gray;
                data[i].green = gray;
                data[i].blue = gray;
            }
        int r = stbi_write_png(
            "natalie_gris.png",
            ancho,
            altura,
            3,
            data,
            3 * ancho);
        if (r) {
            printf("Sí se pudo!");
        } else {
            printf(":'(");
        }
        
        stbi_image_free(data);    
    } else {
        fprintf(stderr, "No se pudo leer el archivo!");  
    }
    
    return 0;
}