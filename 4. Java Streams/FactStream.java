// Cálculo de factorial usando streams de Java 8.

package mx.itesm.cem.pmultinucleo;

import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.stream.IntStream;

public class FactStream {

    public static void main(String[] args) {
        final int n = 250_000;
        Instant start = Instant.now();
        BigInteger r =
                IntStream.rangeClosed(2, n)
                    .parallel()
                    .mapToObj(BigInteger::valueOf)
                    .reduce(BigInteger.ONE, (x, y) -> x.multiply(y));
        Instant end = Instant.now();
        Duration delta = Duration.between(start, end);
        System.out.println(r.bitCount());
        System.out.println(delta.toMillis() / 1000.0);
    }

}
