// Ejemplo de uso de streams en Java 8.

package mx.itesm.cem.pmultinucleo;

import java.util.Optional;
import java.util.stream.Stream;

public class EjemploStream {

    public static void main(String[] args) {
        Stream<String> st1 = Stream.of("enero", "febrero",
                "marzo", "abril", "mayo", "junio", "julio",
                "agosto", "septiembre", "octubre", "noviembre",
                "diciembre");
        Stream<String> st2 = Stream.of("manzana", "guayaba",
                "mango", "plátano", "sandía", "melón");
        Stream<String> st3 = Stream.concat(st1, st2);
        
        // Imprimir todos los elementos de st3.
        //st3.forEach(System.out::println);
        
        // Imprimir el total de elementos de st3.
        //System.out.println(st3.count());
        
        // Imprimir todos los elementos de st3 en mayúsculas.
        //st3.map(String::toUpperCase).forEach(System.out::println);
        
        /*
        // Obtener el elemento más largo de st3.
        Optional<String> r = st3.max((a, b) -> a.length() - b.length());
        if (r.isPresent()) {
            System.out.println(r.get());
        } else {
            System.out.println("No hay");
        }
        */
        
        /*
        // Contar el total de caracteres de todos los elementos de st3.
        // Versión 1 (convirtiendo primero st3 a un stream de enteros)
        int r = st3.mapToInt(String::length).sum();
        System.out.println(r);
        */
        
        // Contar el total de caracteres de todos los elementos de st3.
        // Versión 2 (usando método reduce directamente)
        int r = st3.reduce(0, 
                (accum, x) -> accum + x.length(), 
                (x, y) -> x + y);
        System.out.println(r);
    }
}
