// Demostrando el uso de lambdas e interfaces funcionales 
// en Java 8.

package mx.itesm.cem.pmultinucleo;

import java.util.function.IntUnaryOperator;

@FunctionalInterface
interface MyFunInter {
    public abstract int f(int x);
}

public class EjemploLambda {

    public static void main(String[] args) {
        MyFunInter g = x -> x + 1;
        MyFunInter h = new MyFunInter() {
            @Override
            public int f(int x) {
                return x + 1;
            }
        };
        System.out.println(g.f(5));
        System.out.println(h.f(5));
        
        IntUnaryOperator i = x -> x + 1;
        System.out.println(i.applyAsInt(5));
    }

}
