// Calcula el factorial utilizando Fork/Join de Java.

package mx.itesm.cem.pmultinucleo;

import java.math.BigInteger;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;

public class FactorialTask extends RecursiveTask<BigInteger> {
    
    private final int start;
    private final int end;
    
    private static final int UMBRAL = 1000;
    
    public FactorialTask(int start, int end) {
        this.start = start;
        this.end = end;
    }
    
    @Override
    public BigInteger compute() {
        if (end - start < UMBRAL) {
            BigInteger resultado = BigInteger.ONE;
            for (int i = start; i < end; i++) {
                resultado = resultado.multiply(BigInteger.valueOf(i));
            }
            return resultado;
        } else {
            int mid = (start + end) >>> 1;
            FactorialTask t1, t2;
            t1 = new FactorialTask(start, mid);
            t2 = new FactorialTask(mid, end);
            t1.fork();
            return t2.compute().multiply(t1.join());
        }
    }
    
    public static void main(String[] args) {
        final int n = 500_000;
        FactorialTask task = new FactorialTask(2, n + 1);
        ForkJoinPool pool = new ForkJoinPool();
        BigInteger resultado = pool.invoke(task);
        System.out.println(resultado.bitCount());
    }
    
}