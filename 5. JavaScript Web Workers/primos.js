// Cálculo de números primos usando Web Workers.

'use strict';

$(() => {
  let w = new Worker('primos_worker.js');
  w.onmessage = (event) => {
    $('#resultado').text(event.data);
  };
});
