// Cálculo de números primos usando Web Workers.

'use strict';

let n = 1;

busca: while (true) {
  n++;
  for (let i = 2, max = Math.sqrt(n); i <= max; i++) {
    if (n % i === 0) {
      continue busca;
    }
  }
  postMessage(n);
}