% Un experimento de cómputo paralelo.

-module(experimento).
-compile(export_all).

fibo(X) when X < 2 -> X;
fibo(X) when X > 1 -> fibo(X - 1) + fibo(X - 2).

rangos(NumProcs, NumRects) ->
  T = NumRects div NumProcs,
  lists:map(
    fun (X) -> {X, X + T} end,
    lists:seq(0, NumRects - 1, T)).
    
calcula_rango(Inicio, Fin, Width, Sum) when Inicio < Fin ->
  Mid = (Inicio + 0.5) * Width,
  Height = 4.0 / (1.0 + Mid * Mid),
  calcula_rango(Inicio + 1, Fin, Width, Sum + Height);
calcula_rango(X, X, _, Sum) -> Sum.

calcula_pi(NumProcs, NumRects) ->
  Width = 1 / NumRects,
  lists:sum(
    plists:map(
      fun ({Inicio, Fin}) ->
        calcula_rango(Inicio, Fin, Width, 0)
      end,
      rangos(NumProcs, NumRects))) * Width.
      
% Ejemplos con MapReduce

meses() -> [
  "enero", "febrero", "marzo", "abril",
  "mayo", "junio", "julio", "agosto",
  "septiembre", "octubre", "noviembre", "diciembre"
].

separacion_por_tamano() ->
  dict:to_list(
    plists:mapreduce(fun (X) -> {length(X), X} end, meses())).
    
separacion_por_letra_incial() ->
  dict:to_list(
    plists:mapreduce(fun (X) -> {[hd(X)], X} end, meses())).

  