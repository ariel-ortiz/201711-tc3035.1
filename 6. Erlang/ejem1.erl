% Primer ejemplo de funciones usando Erlang.

-module(ejem1).
-compile(export_all).

pow(_B, 0) ->
  1;
pow(B, E) when E > 0 ->
  B * pow(B, E - 1).
  
dup([]) ->
  [];
dup([H|T]) ->
  [H, H | dup(T)].
  
add1([]) ->
  [];
add1([H|T]) ->
  [H + 1 | add1(T)].
  
countdown(0) ->
  [];
countdown(N) when N > 0 ->
  [N | countdown(N - 1)].
  
log2(1) ->
  0;
log2(N) when N > 1 ->
  1 + log2(N div 2).
  
mimax(N, M) when N > M ->
  N;
mimax(_N, M) ->
  M.
  
largest([X]) ->
  X;
largest([H|T]) ->
  mimax(H, largest(T)).
  
largest2([X]) ->
  X;
largest2([A, B | T]) when A > B ->
  largest2([A | T]);
largest2([_A, B | T]) ->
  largest2([B | T]).
  
largest3([X]) ->
  X;
largest3([A, B | T]) ->
  if
    A > B -> largest3([A | T]);
    true ->  largest3([B | T])
  end.
  