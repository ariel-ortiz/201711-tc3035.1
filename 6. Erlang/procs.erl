% Solución a los primeros problemas de la práctica 6.

-module(procs).
-compile(export_all).

factorial(N) ->
  P = spawn(fun aux/0),
  P ! {fact, N, self()},
  receive
    Result -> Result
  end.
  
aux() ->
  receive
    {fact, N, Remitente} -> Remitente ! fact_fun(N)
  end.
  
fact_fun(0) -> 1;
fact_fun(N) when N > 0 ->
  N * fact_fun(N - 1).
  
fibo_proc() ->
  spawn(fun () -> fibo_loop([1, 0]) end).
  
fibo_send(Pid, Mssg) ->
  case is_process_alive(Pid) of
    true ->
      Pid ! {Mssg, self()},
      receive
        X -> X
      end;
    false -> killed
  end.
  
fibo_loop(Nums) ->
  receive
    {recent, Remitente} ->
      Remitente ! hd(Nums),
      fibo_loop(Nums);
    {span, Remitente} ->
      Remitente ! length(Nums),
      fibo_loop(Nums);
    {_, Remitente} ->
      Remitente ! killed
    after 1000 ->
      [A, B | T] = Nums,
      fibo_loop([A + B, A, B | T])
  end.