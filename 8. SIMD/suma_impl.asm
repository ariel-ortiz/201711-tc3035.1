section .text

; int suma(int a, int b);
; a = rdi
; b = rsi
global suma
    
suma:
    mov rax, rdi ; rax <- rdi
    add rax, rsi ; rax <- rax + rsi
    ret