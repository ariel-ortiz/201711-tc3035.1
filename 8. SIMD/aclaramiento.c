#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <limits.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define NANOSECS_PER_SEC 1000000000

long get_time() {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec * NANOSECS_PER_SEC + t.tv_nsec;
}

extern void aclaramiento_simd(
    uint8_t * data,
    int num_pixels,
    uint8_t * constante_aclaramiento);

void aclaramiento_secuencial(
    uint8_t * data,
    int num_pixels,
    int constante_aclaramiento) {
  
  for (int i = 0; i < num_pixels; i++) {
      int suma = data[i] + constante_aclaramiento;
      if (suma > UCHAR_MAX) {
          data[i] = UCHAR_MAX;
      } else {
          data[i] = (uint8_t) suma;
      }
  }
  
}

int main(void) {
    int ancho, altura, canales;
    uint8_t *data = (uint8_t *) stbi_load(
        "scarlet.png", &ancho, &altura, &canales, 0);
    if (data != NULL) {
        printf("Ancho = %d\n", ancho);
        printf("Alto = %d\n", altura);
        printf("Canales = %d\n", canales);
        int num_pixels = ancho * altura;
        
        long inicio = get_time();
        aclaramiento_secuencial(data, num_pixels, 100);
        long final = get_time();
        printf("T secuencial = %.4f\n",
               ((double) final - inicio) / NANOSECS_PER_SEC);
        int r = stbi_write_png(
            "scarlet_aclarada1.png",
            ancho,
            altura,
            1,
            data,
            ancho);
        if (r) {
            printf("Sí se pudo!\n");
        } else {
            printf(":'(");
        }
        
        uint8_t cte[16];
        for (int i = 0; i < 16; i++) {
            cte[i] = 100;
        }
        inicio = get_time();
        aclaramiento_simd(data, num_pixels, cte);
        final = get_time();
        printf("T SIMD = %.4f\n",
               ((double) final - inicio) / NANOSECS_PER_SEC);
        
        r = stbi_write_png(
            "scarlet_aclarada2.png",
            ancho,
            altura,
            1,
            data,
            ancho);
        if (r) {
            printf("Sí se pudo!\n");
        } else {
            printf(":'(");
        }
    }
    stbi_image_free(data);
    return 0;
}