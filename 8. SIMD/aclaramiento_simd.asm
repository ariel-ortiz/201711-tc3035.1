section .text

;extern void aclaramiento_simd(
;    uint8_t * data,                     ;;; rdi
;    int num_pixels,                     ;;; rsi
;    unint8_t * constante_aclaramiento   ;;; rdx
;  );

global aclaramiento_simd

aclaramiento_simd:
    movdqu xmm0, [rdx]
    shr rsi, 4   ; rsi <- rsi / 16
.ciclo:
    movdqu xmm1, [rdi]
    paddusb xmm1, xmm0  ; xmm1 <- xmm1 + xmm0
    movdqu [rdi], xmm1
    add rdi, 16
    dec rsi
    jnz .ciclo
    ret
    
    
    