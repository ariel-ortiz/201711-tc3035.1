# Tc3035.1 Programación multinúcleo

Este proyecto contiene todos los archivos fuente que el profesor irá generando en clase durante el semestre **enero-mayo de 2017**.

Los archivos se pueden consultar directamente en este mismo sitio, o se puede usar `git` para obtener una copia local de éstos. En este último caso se deben seguir los siguiente pasos:

 1. Si es necesario, [instalar un cliente git](http://git-scm.com/downloads) en tu computadora.

 2. Clonar este repositorio. Desde una ventana de terminal (`cmd.exe` en Windows) teclear:
    
        git clone https://bitbucket.org/ariel-ortiz/201711-tc3035.1.git tc3035
    
 3. Cambiarse al directorio `tc3035`:
    
        cd tc3035
    
    En dicho directorio encontrarás todos los archivos fuente del proyecto.
    
 4. Cada vez que el profesor realice modificaciones a archivos existentes o agregue archivos nuevos, será necesario hacer un *pull* al repositorio. En la terminal y desde el directorio `tc1014` teclear: 
    
        git pull